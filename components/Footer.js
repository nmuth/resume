import React from 'react'

class CopyrightYear extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      revealed: false,
    }
  }

  revealTheFun = () => {
    this.setState({ revealed: true })

    setTimeout(() => {
      this.setState({ revealed: false })
    }, 1000)
  }

  render() {
    const { revealed } = this.state

    if (revealed) {
      return (
        <span>{new Date().getFullYear()}</span>
      )
    }

    return (
      <span onMouseEnter={this.revealTheFun} onClick={this.revealTheFun}>$CURRENT_YEAR</span>
    )
  }
}

export default () => (
  <footer>
    <style jsx>{`
      footer {
        font-weight: lighter;
        font-size: 0.8em;
        margin-top: 3em;
        text-align: center;
        border-top: 1px solid #e5e5e5;
        padding-top: 1em;
        padding-bottom: 0.5em;
      }
      `}</style>

    © Noah Muth <CopyrightYear/> ·
    made with <a href="https://nextjs.org/docs" target="_blank">Next.js</a>,
  served by <a href="https://netlify.com" target="_blank">Netlify</a>
  </footer>
)
