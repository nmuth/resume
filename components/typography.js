export const fonts = {
  heading: 'Helvetica Neue, sans-serif',
  body: `'Muli', 'Open Sans', sans-serif`,
}

export const Global = () => (
  <style global jsx>{`
    html, body {
      font-family: ${fonts.body};
      font-size: 11pt;
      background-color: #fefdff;
    }

    p, li, a, span, tbody {
      margin-top: 0.5em;
    }

    h1,h2,h3,h4,h5 {
      font-family: ${fonts.heading};
      font-weight: 500;
    }

    h1 {
      font-size: 2em;
      font-weight: normal;
    }

    h2 {
      letter-spacing: 2px;
      font-size: 1em;
      text-transform: uppercase;
      margin-top: 2em;
      margin-bottom: 0.5em;
    }
    `}</style>
)

export const BodyList = ({ items }) => (
  <ul>
    <style jsx>{`
      // make the list not ugly later
      `}</style>

    {items.map((i, idx) => (<li key={idx}>{i}</li>))}
  </ul>
)
