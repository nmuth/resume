const tableOffsetColor = '#F2EFEC'

export default ({ data }) => {
  const rows = Object.keys(data).map((type, idx) => {
    const isEven = idx % 2 === 0

    const trStyle = isEven
                  ? {}
                  : { backgroundColor: tableOffsetColor }

    const tdStyle = {
      padding: '0.5em',
    }

    const tools = data[type]

    return (
      <tr style={trStyle} key={idx}>
        <td style={tdStyle}>{type}</td>
        <td style={tdStyle}>{tools.join(', ')}</td>
      </tr>
    )
  })

  return (
    <table cellSpacing="0">
      <style jsx>{`
        table {
          width: 100%;
          border: 1px solid ${tableOffsetColor};
        }
        `}</style>

      <tbody>
        {rows}
      </tbody>
    </table>
  )
}
