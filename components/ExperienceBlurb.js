const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

export default ({ jobTitle = '', company = '', startDate = null, endDate = null, highlights = [] }) => {
  const formattedStart = startDate && `${months[startDate.getMonth()]} ${startDate.getFullYear()}`
  const formattedEnd = endDate && `${months[endDate.getMonth()]} ${endDate.getFullYear()}`
  const formattedRange = formattedStart && formattedEnd && `${formattedStart} – ${formattedEnd}`

  let header = [jobTitle, company].filter(x => x).join(', ')

  if (formattedRange) {
    header += header ? '; ' : ''
    header += formattedRange
  }

  return (
    <div className="experienceBlurb">
    <style jsx>{`
      h3 {
        font-size: 1em;
        font-weight: normal;
        font-style: italic;
        margin-bottom: 0.5em;
      }

      ul {
        margin-top: 0.5em;
      }
      `}</style>

      <h3>
        {header}
      </h3>
      <ul>
        {highlights.map((text, idx) => <li key={idx}>{text}</li>)}
      </ul>
    </div>
  )
}
