import Head from 'next/head'
import ExperienceBlurb from '../components/ExperienceBlurb'
import Footer from '../components/Footer'
import ToolboxTable from '../components/Toolbox'
import { BodyList, Global } from '../components/typography'

const toolbox = {
  'Frontend Languages': ['ECMAScript/JavaScript', 'TypeScript', 'Elm'],
  'Frontend Libraries': ['React', 'Redux', 'Vue', 'Vuex', 'Ember', 'Angular 2+'],
  'Mobile (iOS + Android)': ['React Native', 'Expo'],
  'Backend Languages': ['Node', 'Go', 'Python', 'Elixir/Erlang/OTP', 'Ruby'],
  'Databases': ['PostgreSQL', 'MongoDB', 'MySQL', 'Redis', 'Google Datastore'],
  'Tooling': ['git', 'GitHub', 'GitLab', 'CircleCI', 'Drone', 'Jenkins'],
  'Deployment': ['Kubernetes', 'Docker', 'Google Cloud', 'AWS', 'Netlify'],
  'Other': ['emacs, Lisp, Unity 3D, Godot'],
}

const consultingHighlights = [
  'Led an international team through the migration of a monolithic REST API to immutable microservices and personally deployed the new system to production',
  'Applied human-first design principles to user interfaces to create aesthetically pleasing applications without sacrificing usability',
  'Created multiple WYSIWYG editors for print media from the ground up, including UI/UX design, technical architecture, and actual implementation',
  'Mentored junior developers 1-on-1 to build their general engineering skills as well as proficiencies with specific technology',
  'Designed, specced, prioritized, and implemented product features based on real-world user experiences and pain points',
  'Architected robust, scalable, secure microservice systems for enterprise data processing',
]

const nonConsultingExperience = [{
  jobTitle: 'Engineer III',
  company: 'CrowdStrike',
  startDate: new Date('2018-08-01'),
  endDate: new Date('2019-04-01'),
  highlights: [
    'Collaborated with remote team to build cutting-edge security software and stop breaches',
    'Developed internal tools to assist in administration, deployment, and support of the Falcon platform',
    'Coordinated with team members across disciplines to deliver effective solutions',
  ],
},{
  jobTitle: 'Software Engineer',
  company: 'Code42 Software',
  startDate: new Date('2015-10-01'),
  endDate: new Date('2016-08-01'),
  highlights: [
    'Planned and implemented refactoring efforts to modernize an aging JQuery UI codebase',
    'Developed web-based administrative tools with ES6, React, and Angular',
    'Created comprehensive user acceptance tests with Cucumber and Capybara',
  ],
},{
  jobTitle: 'Developer Intern',
  company: 'Maverick Software Consulting',
  startDate: new Date('2014-02-01'),
  endDate: new Date('2014-08-01'),
  highlights: [
    'Maintained and extended Typescript and Java code for web-based admin tools',
    'Supported agile and test-driven development practices',
    'Ensured HIPAA compliance when working with healthcare PII',
  ],
},{
  jobTitle: 'Organizer Intern',
  company: 'Minnesotans United for All Families',
  startDate: new Date('2012-04-01'),
  endDate: new Date('2012-11-01'),
  highlights: [
    'Canvassed phones and doors to defend LGBTQ rights in Minnesota',
    'Recruited & trained volunteers to participate in phone banks for persuasion and GOTV',
    'Organized fundraising & awareness events to help sustain the campaign',
    'We won :)',
  ],
}]
  .sort((a, b) => {
    return a.startDate < b.startDate
  })

export default () => (
  <div className="container">
    <style jsx>{`
      .container {
        max-width: 50em;
        margin: 0 auto;
        padding: 0 3em;
      }

      header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #e5e5e5;
      }

      header ul {
        font-size: 0.8em;
        list-style: none;
        padding: 0;
      }

      header ul > li {
        margin-top: 0;
      }

      address {
        font-size: 0.8em;
        font-style: normal;
        text-align: right;
      }

      address p {
        margin: 0;
      }

      section p {
        text-align: justify;
      }

      @media (max-width: 550px) {
        header {
          flex-direction: column;
          justify-content: space-between;
          align-items: center;
          padding-bottom: 1em;
        }

        header h1 {
          margin-bottom: 1rem;
        }

        header ul {
          margin-top: 0;
        }

        header address {
          text-align: center;
        }

        section p {
          text-align: left;
        }
      }
      `}</style>

    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1"/>
      <meta charSet="utf-8"/>

      <link href="https://fonts.googleapis.com/css?family=Muli:300,300i&display=swap" rel="stylesheet"/>

      <title>Noah Muth</title>
    </Head>

    <Global/>

    <header>
      <h1>
        Noah Muth
      </h1>
      <ul>
        <li><a href="mailto:noah@fulgid.io">noah@fulgid.io</a></li>
        <li><a href="tel:651-788-1762">(651) 788 1762</a></li>
      </ul>
      <address>
        <p>101 S 5th St #1419</p>
        <p>Minneapolis, MN 55402</p>
      </address>
    </header>
    <main>
      <section>
        <h2>Who I Am</h2>
        <p>
          A consulting technical lead based out of Minneapolis, MN with
          experience across a variety of languages, frameworks, and work
          environments. When I’m not on the computer, I love to travel, write
          music, and study the Dutch language.
        </p>
        <h2>What I Believe</h2>
        <p>
          That people are more important than software; that happy teams build
          better products; and that the most crucial function of my job is to be
          a helpful team member, curious learner, and effective teacher.
        </p>
        <h2>What I Do</h2>
        <p>
          I turn ideas into products—REST APIs, microservices, web & mobile
          applications, games, etc.—using whatever tool is most appropriate for
          the job. I’m always on the lookout for new (or old!) solutions to add
          to my repertoire.
        </p>
      </section>
      <section>
        <h2>Toolbox</h2>
        <ToolboxTable data={toolbox}/>
      </section>
      <section>
        <h2>Consulting Highlights (July 2014 - Present)</h2>
        <p>
          As a consultant, I’ve partnered with a variety of clients from scrappy
          bedroom startups to B2B enterprises and retail titans. I have been
          consulting since July 2014, and have worked on far too many projects
          to enumerate exhaustively. These are my proudest accomplishments as a
          consulting technical lead.
        </p>
        <BodyList items={consultingHighlights}/>
      </section>
      <section>
        <h2>Non-Consulting Experience</h2>
        {nonConsultingExperience.map((exp, idx) => <ExperienceBlurb key={idx} {...exp}/>)}
      </section>
    </main>
    <Footer/>
  </div>
)
